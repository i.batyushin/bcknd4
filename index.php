<?php
/**
 * Реализовать проверку заполнения обязательных полей формы в предыдущей
 * с использованием Cookies, а также заполнение формы по умолчанию ранее
 * введенными значениями.
 */

// Отправляем браузеру правильную кодировку,
// файл index.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');

// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  // Массив для временного хранения сообщений пользователю.
  $messages = array();

  // В суперглобальном массиве $_COOKIE PHP хранит все имена и значения куки текущего запроса.
  // Выдаем сообщение об успешном сохранении.
  if (!empty($_COOKIE['save'])) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('save', '', 100000);
    // Если есть параметр save, то выводим сообщение пользователю.
    $messages[] = 'Спасибо, результаты сохранены.';
  }

  // Складываем признак ошибок в массив.
  $errors = array();
  $errors['field-name-1'] = !empty($_COOKIE['field-name-1_error']);
  $errors['field-email'] = !empty($_COOKIE['field-email_error']);
  $errors['field-date'] = !empty($_COOKIE['field-date_error']);
  $errors['field-name-3'] = !empty($_COOKIE['field-name-3_error']);
  $errors['field-name-2'] = !empty($_COOKIE['field-name-2_error']);
  $errors['field-name-4'] = !empty($_COOKIE['field-name-4_error']);
  $errors['radio-group-2'] = !empty($_COOKIE['radio-group-2_error']);
  $errors['radio-group-1'] = !empty($_COOKIE['radio-group-1_error']);
  
  // TODO: аналогично все поля.

  // Выдаем сообщения об ошибках.
  if ($errors['field-name-1']) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('field-name-1_error', '', 100000);
    // Выводим сообщение.
    $messages[] = '<div class="error">Заполните имя корректно.</div>';
  }
  // TODO: тут выдать сообщения об ошибках в других полях.
  if ($errors['field-email']) {
    setcookie('field-email_error', '', 100000);
    $messages[] = '<div class="error">Заполните электронный адрес корректно.</div>';
  }
  if ($errors['field-date']) {
    setcookie('field-date_error', '', 100000);
    $messages[] = '<div class="error">Заполните дату.</div>';
  }
  if ($errors['field-name-3']) {
    setcookie('field-name-3_error', '', 100000);
    $messages[] = '<div class="error">Выберите суперспособность.</div>';
  }
  if ($errors['field-name-2']) {
    setcookie('field-name-2_error', '', 100000);
    $messages[] = '<div class="error">Заполните информацию о себе через 10 лет.</div>';
  }
  if ($errors['field-name-4']) {
    setcookie('field-name-4_error', '', 100000);
    $messages[] = '<div class="error">Выберите пиццу.</div>';
  }
  if ($errors['radio-group-2']) {
    setcookie('radio-group-2_error', '', 100000);
    $messages[] = '<div class="error">Поставьте оценку.</div>';
  }
  if ($errors['radio-group-1']) {
    setcookie('radio-group-1_error', '', 100000);
    $messages[] = '<div class="error">Укажите пол.</div>';
  }

  // Складываем предыдущие значения полей в массив, если есть.
  $values = array();
  $values['field-name-1'] = empty($_COOKIE['field-name-1_value']) ? '' : $_COOKIE['field-name-1_value'];
  // TODO: аналогично все поля.
  $values['field-email'] = empty($_COOKIE['field-email_value']) ? '' : $_COOKIE['field-email_value'];
  $values['field-date'] = empty($_COOKIE['field-date_value']) ? '' : $_COOKIE['field-date_value'];
  $values['field-name-3'] = empty($_COOKIE['field-name-3_value']) ? '' : $_COOKIE['field-name-3_value'];
  $values['field-name-2'] = empty($_COOKIE['field-name-2_value']) ? '' : $_COOKIE['field-name-2_value'];
  $values['field-name-4'] = empty($_COOKIE['field-name-4_value']) ? '' : $_COOKIE['field-name-4_value'];
  $values['radio-group-2'] = empty($_COOKIE['radio-group-2_value']) ? '' : $_COOKIE['radio-group-2_value'];
  $values['radio-group-1'] = empty($_COOKIE['radio-group-1_value']) ? '' : $_COOKIE['radio-group-1_value'];
  

  // Включаем содержимое файла form.php.
  // В нем будут доступны переменные $messages, $errors и $values для вывода 
  // сообщений, полей с ранее заполненными данными и признаками ошибок.
  include('form.php');
}
// Иначе, если запрос был методом POST, т.е. нужно проверить данные и сохранить их в XML-файл.
else {
  // Проверяем ошибки.
  $errors = FALSE;
  if (empty($_POST['field-name-1']) || preg_match('/^[^<,\"@/{}()*$%?=>:|;#]*$/i', $_POST['field-name-1'])) {
    // Выдаем куку на день с флажком об ошибке в поле fio.
    setcookie('field-name-1_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('field-name-1_value', $_POST['field-name-1'], time() + 30 * 24 * 60 * 60);
  }

// *************
// TODO: тут необходимо проверить правильность заполнения всех остальных полей.
// Сохранить в Cookie признаки ошибок и значения полей.
// *************
if (empty($_POST['field-email']) || preg_match('/[^(\w)|(\@)|(\.)|(\-)]/',$_POST['field-email'])) {
  setcookie('field-email_error', '1', time() + 24 * 60 * 60);
  $errors = TRUE;
}
else {
  setcookie('field-email_value', $_POST['field-email'], time() + 30 * 24 * 60 * 60);
} if (empty($_POST['field-date'])) {
  setcookie('field-date_error', '1', time() + 24 * 60 * 60);
  $errors = TRUE;
}
else {
  setcookie('field-date_value', $_POST['field-date'], time() + 30 * 24 * 60 * 60);
} if (empty($_POST['field-name-3'])) {
  setcookie('field-name-3_error', '1', time() + 24 * 60 * 60);
  $errors = TRUE;
}
else {
  setcookie('field-name-3_value', $_POST['field-name-3'], time() + 30 * 24 * 60 * 60);
} if (empty($_POST['field-name-2'])) {
  setcookie('field-name-2_error', '1', time() + 24 * 60 * 60);
  $errors = TRUE;
}
else {
  setcookie('field-name-2_value', $_POST['field-name-2'], time() + 30 * 24 * 60 * 60);
} if (empty($_POST['field-name-4'])) {
  setcookie('field-name-4_error', '1', time() + 24 * 60 * 60);
  $errors = TRUE;
}
else {
  setcookie('field-name-4_value', $_POST['field-name-4'], time() + 30 * 24 * 60 * 60);
} if (empty($_POST['radio-group-2'])) {
  setcookie('radio-group-2_error', '1', time() + 24 * 60 * 60);
  $errors = TRUE;
}
else {
  setcookie('radio-group-2_value', $_POST['radio-group-2'], time() + 30 * 24 * 60 * 60);
} if (empty($_POST['radio-group-1'])) {
  setcookie('radio-group-1_error', '1', time() + 24 * 60 * 60);
  $errors = TRUE;
}
else {
  setcookie('radio-group-1_value', $_POST['radio-group-1'], time() + 30 * 24 * 60 * 60);
}

  if ($errors) {
    // При наличии ошибок перезагружаем страницу и завершаем работу скрипта.
    header('Location: index.php');
    exit();
  }
  else {
    // Удаляем Cookies с признаками ошибок.
    setcookie('field-name-1_error', '', 100000);
    // TODO: тут необходимо удалить остальные Cookies.
    setcookie('field-email_error', '', 100000);
    setcookie('field-date_error', '', 100000);
    setcookie('field-name-3_error', '', 100000);
    setcookie('field-name-2_error', '', 100000);
    setcookie('field-name-4_error', '', 100000);
    setcookie('radio-group-2_error', '', 100000);
    setcookie('radio-group-1_error', '', 100000);
  }
  $user = 'u24234';
  $pass = '43523453';
  $db = new PDO('mysql:host=localhost;dbname=u24234', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
  $Vibor = implode(',',$_POST['field-name-4']);
  // Подготовленный запрос. Не именованные метки.
  try {
    $stmt = $db->prepare("INSERT INTO application_v2 SET name = ?, email = ?, DateOfBirth = ?, SPower = ?, After10years = ?, OcenkaPitsi = ?, Gender = ?");
    $stmt -> execute([$_POST['field-name-1'],$_POST['field-email'],$_POST['field-date'],$_POST['field-name-3'],$_POST['field-name-2'],$_POST['radio-group-2'],$_POST['radio-group-1']]);
    $mas=explode(',',$Vibor);
    $stmt = $db->prepare("INSERT INTO Pizza SET user_id = ?, nomer_pitsi = ?");
    foreach($mas as $el){
      $link=mysqli_connect('localhost',$user,$pass,'u24234');
      $email=$_POST['field-email'];
      $exodus=mysqli_query($link,"SELECT id from application_v2 WHERE email='$email'");
      $strid=mysqli_fetch_row($exodus);
      $id=(int) $strid[0];
      $stmt -> execute([$id,$el]);
    }  
  }
  catch(PDOException $e){
    print('Error : ' . $e->getMessage());
    exit();
  }
  // Сохранение в XML-документ.
  // ...

  // Сохраняем куку с признаком успешного сохранения.
  setcookie('save', '1');

  // Делаем перенаправление.
  header('Location: index.php');
}
